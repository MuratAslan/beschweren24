<?php
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Subcategory
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(nullable=true)
     */
    private $SubcategoryName;

    /**
     * @ORM\OneToMany(targetEntity="Models", mappedBy="subcategory")
     */
    private $products;

    /**
     * @ORM\ManyToOne(targetEntity="Categories", inversedBy="subcategory")
     * @ORM\JoinColumn(name="categories_id", referencedColumnName="id")
     */
    private $categories;
}