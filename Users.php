<?php
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Users
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $UserName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Password;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Mail;

    /**
     * @ORM\Column(type="integer", length=1, nullable=true)
     */
    private $Rol;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $Active;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ActiveKey;

    /**
     * @ORM\OneToOne(targetEntity="UserDetails", mappedBy="users")
     */
    private $userDetails;

    /**
     * @ORM\OneToMany(targetEntity="Companies", mappedBy="users")
     */
    private $companies;

    /**
     * @ORM\OneToMany(targetEntity="Complaints", mappedBy="users")
     */
    private $complaints;

    /**
     * @ORM\OneToMany(targetEntity="OldPassword", mappedBy="users")
     */
    private $oldPassword;
}