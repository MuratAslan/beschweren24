<?php
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class UserDetails
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Surname;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $DateOfBirth;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Telephone1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Telephone2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ProfileImage;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $DateOfUpdate;

    /**
     * @ORM\OneToOne(targetEntity="Users", inversedBy="userDetails")
     * @ORM\JoinColumn(name="users_id", referencedColumnName="id", unique=true)
     */
    private $users;
}