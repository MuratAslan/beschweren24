<?php
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Models
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ModelName;

    /**
     * @ORM\OneToMany(targetEntity="Attributes ", mappedBy="products")
     */
    private $attributes ;

    /**
     * @ORM\ManyToOne(targetEntity="Subcategory", inversedBy="products")
     * @ORM\JoinColumn(name="subcategory_id", referencedColumnName="id")
     */
    private $subcategory;

    /**
     * @ORM\ManyToOne(targetEntity="Companies", inversedBy="products")
     * @ORM\JoinColumn(name="companies_id", referencedColumnName="id")
     */
    private $companies;

    /**
     * @ORM\ManyToOne(targetEntity="Complaints", inversedBy="products")
     * @ORM\JoinColumn(name="complaints_id", referencedColumnName="id")
     */
    private $complaints;
}