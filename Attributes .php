<?php
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Attributes 
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $AttributeName;

    /**
     * @ORM\ManyToOne(targetEntity="Types", inversedBy="attributes ")
     * @ORM\JoinColumn(name="types_id", referencedColumnName="id")
     */
    private $types;

    /**
     * @ORM\ManyToOne(targetEntity="Models", inversedBy="attributes ")
     * @ORM\JoinColumn(name="products_id", referencedColumnName="id")
     */
    private $products;
}