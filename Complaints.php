<?php
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Complaints
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $ComplaintTitle;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $Complaint;

    /**
     * @ORM\OneToMany(targetEntity="Models", mappedBy="complaints")
     */
    private $products;

    /**
     * @ORM\ManyToOne(targetEntity="Users", inversedBy="complaints")
     * @ORM\JoinColumn(name="users_id", referencedColumnName="id")
     */
    private $users;
}