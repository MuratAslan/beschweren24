<?php
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class OldPassword
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Password;

    /**
     * @ORM\ManyToOne(targetEntity="Users", inversedBy="oldPassword")
     * @ORM\JoinColumn(name="users_id", referencedColumnName="id")
     */
    private $users;
}